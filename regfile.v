`default_nettype none
`timescale 1ns/1ps

module regfile (
	input wire clk, we,
	input wire [4:0] r1addr, r2addr, waddr,
	input wire [31:0] wdata,
	output wire [31:0] r1data, r2data
);

reg [31:0] registers [1:31];

/* Async Read on second half of cycle */
assign r1data = registers[r1addr];
assign r2data = registers[r2addr];

/* Async write at the beginning of the cycle */
always @ (negedge clk) begin
	if (we == 1'b1 && waddr != 5'b0) begin
		registers[waddr] <= wdata;
	end
end

// Debugging Access Functions
`ifdef verilator
	export "DPI-C" task get_reg_value;
	task get_reg_value;
		input bit [4:0] get_addr;
		output bit [31:0] reg_val;
		begin
			reg_val = registers[get_addr];
		end
	endtask
`endif

endmodule
