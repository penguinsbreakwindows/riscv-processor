datapath_test:
	verilator --trace --cc --exe --build -j 0 -Wall tbdatapath.cpp datapath.v
	./obj_dir/Vdatapath
datapath_synth:
	yosys -p "read_verilog datapath.v ; hierarchy -top datapath -libdir . ; synth_ecp5"
clean:
	rm -rf log_* obj_dir
alu_test:
	verilator --cc --exe --build -j 0 -Wall tbalu.cpp alu.v
	./obj_dir/Valu
