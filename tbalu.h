#ifndef ALUOP
#define ALUOP

#define ADD             0b0000
#define SUB             0b1000
#define XOR             0b0010
#define OR              0b0110
#define AND             0b0111
#define SLL             0b0001
#define SRL             0b0101
#define SRA             0b1101
#define SLT             0b0010
#define SLTU            0b0011

#endif
