`default_nettype none
`timescale 1ns/1ps

module decode
(
	input wire [6:2] opcode,
	input wire [2:0] func3,
	input wire func7,

	output reg wb_pc_d,
	output reg wb_mem_d,

	output reg wr_mem_d,
	output reg wr_reg_d,

	output reg [4:0] alu_op_d,
	output reg alu_1_d,
	output reg alu_2_d,

	output reg branch_d,
	output reg jump_d,
	output reg jump_r_d,

	output reg [3:0] i_b_j_u
);

always @ (*) begin
	wb_pc_d  = 1'bx;
	wb_mem_d = 1'bx;
	wr_mem_d = 1'bx;
	wr_reg_d = 1'bx;
	alu_op_d = 5'bxxxxx;
	alu_1_d  = 1'bx;
	alu_2_d  = 1'bx;
	branch_d = 1'bx;
	jump_d   = 1'bx;
	jump_r_d = 1'bx;
	i_b_j_u  = 4'bxxxx;
	case (opcode[6:2])
		5'b01100: // R Type
		begin
			wb_mem_d = 1'b0;
			wr_mem_d = 1'b0;
			wr_reg_d = 1'b1;
			alu_op_d = {1'b0, func7, func3};
			alu_1_d  = 1'b1;
			alu_2_d  = 1'b1;
			branch_d = 1'b0;
			jump_d   = 1'b0;
			i_b_j_u  = 4'bxxxx;
		end
		5'b00100: // Math I Type
		begin
			wb_mem_d = 1'b0;
			wr_mem_d = 1'b0;
			wr_reg_d = 1'b1;
			alu_op_d = {2'b0, func3};
			alu_1_d = 1'b1;
			alu_2_d = 1'b0;
			branch_d = 1'b0;
			jump_d = 1'b0;
			i_b_j_u = 4'b1000;
		end
		5'b00000: // Load I Type
		begin
			wb_mem_d = 1'b1;
			wr_mem_d = 1'b0;
			wr_reg_d = 1'b1;
			alu_op_d = 5'b0;
			alu_1_d  = 1'b1;
			alu_2_d  = 1'b0;
			branch_d = 1'b0;
			jump_d   = 1'b0;
			i_b_j_u  = 4'b1000;
		end
		5'b01000: // S Type
		begin
			wr_mem_d = 1'b1;
			wr_reg_d = 1'b0;
			alu_op_d = 5'b0;
			alu_1_d  = 1'b1;
			alu_2_d  = 1'b0;
			branch_d = 1'b0;
			jump_d   = 1'b0;
			i_b_j_u  = 4'b0;
		end
		5'b11000: // B Type
		begin
			wr_mem_d = 1'b0;
			wr_reg_d = 1'b0;
			alu_op_d = {2'b0, ~func3[2], func3[2], func3[1]};
			alu_1_d  = 1'b1;
			alu_2_d  = 1'b1;
			branch_d = 1'b1;
			jump_d   = 1'b0;
			jump_r_d = 1'b0;
			i_b_j_u  = 4'b0100;
		end
		5'b11011: // JAL
		begin
			wb_pc_d  = 1'b1;
			wr_mem_d = 1'b0;
			wr_reg_d = 1'b1;
			branch_d = 1'b0;
			jump_d   = 1'b1;
			jump_r_d = 1'b0;
			i_b_j_u  =  4'b0010;
		end
		5'b11001: // JALR
		begin
			wb_pc_d  = 1'b1;
			wr_mem_d = 1'b0;
			wr_reg_d = 1'b1;
			branch_d = 1'b0;
			jump_d   = 1'b1;
			jump_r_d = 1'b1;
			i_b_j_u  = 4'b0010;
		end
		5'b01101: // LUI
		begin
			wb_mem_d = 1'b0;
			wr_mem_d = 1'b0;
			wr_reg_d = 1'b1;
			alu_op_d = 5'b10000; // Pass through
			alu_2_d  = 1'b0;
			branch_d = 1'b0;
			jump_d   = 1'b0;
			i_b_j_u  = 4'b0001;
		end
		5'b00101: // AUIPC
		begin
			wb_mem_d = 1'b0;
			wr_mem_d = 1'b0;
			wr_reg_d = 1'b1;
			alu_op_d = 5'b0; //ADD
			alu_1_d  = 1'b0;
			alu_2_d  = 1'b0;
			branch_d = 1'b0;
			jump_d   = 1'b0;
			i_b_j_u  = 4'b0001;
		end
		default: begin
		end
	endcase
end

endmodule
