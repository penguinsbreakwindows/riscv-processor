#include<memory>

#include<iostream>
#include<iomanip>
#include<bitset>

#include <verilated.h>

#include "svdpi.h"

#include "Vdatapath.h"
#include "Vdatapath__Dpi.h"

#include "tbdatapath.h"

int main(int argc, char** argv, char** env) {

	if (false && argc && argv && env) {}

	Verilated::mkdir("log_datapath");

	const std::unique_ptr<VerilatedContext> contextp {new VerilatedContext};
	contextp->commandArgs(argc, argv);
	contextp->debug(0);
	contextp->traceEverOn(true);
	contextp->randReset(2);
	contextp->randReset(2);

	const std::unique_ptr<Vdatapath> top{new Vdatapath{contextp.get(), "TOP"}};

	contextp->timeInc(1);
	top->clk = 0;
	top->rst = 1;

	top->eval();

	contextp->timeInc(1);
	top->clk = 1;
	top->rst = 1;

	top->eval();

	contextp->timeInc(1);
	top->clk = 0;
	top->rst = 0;

	top->eval();

	for (int i = 0; i < 75; i++) {
		contextp->timeInc(1);
		top->clk = !top->clk;
		top->eval();
	}

	const svScope scope = svGetScopeFromName("TOP.datapath.reg0");
	assert(scope);
	svSetScope(scope);

	uint32_t reg_addr;
	uint32_t reg_val;
	for (int i = 1; i < 6; i++) {
		reg_addr = i;
		reg_val = 0;
		top->get_reg_value(&reg_addr, &reg_val);
		std::cout << "Register: " << i << " Value: " << std::hex << reg_val << std::endl;
	}

	top->final();

	return 0;

	
}
