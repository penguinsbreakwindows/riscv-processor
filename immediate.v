`default_nettype none
`timescale 1ns/1ps

module immediate
(
	input wire [31:7] instr,
	input wire i,u,j,b,
	output wire [31:0] imm
);

// Note, can get rid of S signal if needed
assign imm[31] = instr[31];
assign imm[30:20] = u ? instr[30:20] : {11{instr[31]}};
assign imm[19:12] = (u|j) ? instr[19:12] : {8{instr[31]}};
assign imm[11] = ~u & (b ? instr[7] : (j ? instr[20] : instr[31]));
assign imm[10:5] = {6{~u}} & instr[30:25];
assign imm[4:1] = {4{~u}} & (i|j ? instr[24:21] : instr[11:8]);
assign imm[0] = ~(u|j|b) & (i ? instr[20] : instr[7]);

endmodule
