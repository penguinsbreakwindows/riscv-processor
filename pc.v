`default_nettype none
`timescale 1ns/1ps

module pc
(
	input wire we, clk, rst,
	input wire [31:0] pc_new,
	output reg [31:0] pc
);

always @ (posedge clk or posedge rst) begin
	if (rst) begin
		pc <= 0;
	end
	else if (we) begin
		pc <= pc_new;
	end
end

endmodule
