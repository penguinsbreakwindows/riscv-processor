module icache
#(
	/* Defaults to 4KB 64 byte block */
	parameter TAG_WIDTH = 20,
	parameter INDEX_WIDTH = 6,
)
(
	input wire clk,
	input wire [31:0] iaddr,

	output reg valid
	output reg [31:0] idata,
);

localparam BLK_IDX_WIDTH = 32 - TAG_WIDTH - INDEX_WIDTH;

wire [31-INDEX_WIDTH:BLK_IDX_WIDTH] cache_index = iaddr[31-INDEX_WIDTH:BLK_IDX_WIDTH];
wire [31:31-TAG_WIDTH+1] cache_tag = iaddr[31:31-TAG_WIDTH+1];

reg tag_valid [0:1][0:(1 << INDEX_WIDTH)-1];
reg tag_sel_valid [0:1];

reg tag_dirty [0:1][0:(1 << INDEX_WIDTH)-1];
reg tag_sel_dirty [0:1];

reg [TAG_WIDTH - 1:0] tag_data [0:1][0:(1 << INDEX_WIDTH)-1];
reg [TAG_WIDTH - 1:0] tag_sel_data [0:1];

reg [31:0] cache_data [0:1][0:(1 << INDEX_WIDTH)-1];

reg [31:0] data_sel [0:1];

wire [1:0] tag_hit;

always @ (posedge clk) begin
	tag_sel[0] <= tags_data[0][cache_index];
	tag_sel[1] <= tags_data[1][cache_index];

	data_sel[0] <= cache_data[0][cache_index];
	data_sel[1] <= cache_data[1][cache_index];
end

/* Hit if tag match and valid */
assign tag_hit[0] = (|(tag_sel[0] ^ cache_tag)) & tag_sel_valid[0];
assign tag_hit[1] = (|(tag_sel[1] ^ cache_tag)) & tag_sel_valid[1];

always @ (posedge clk) begin
	idata <= data_sel[tag_hit[1]];
	valid <= |tag_hit;
end


endmodule
